#!/bin/bash

# Tags available here https://github.com/kubernetes/kubernetes/tags
TAGS="v1.17.12 v1.17.17 v1.18.16 v1.18.20 v1.19.8 v1.19.15 v1.20.4 v1.20.11"

for TAG in  ${TAGS}; do
  OUTPUT_DIR="${TAG}-standalone"
  if [ ! -d "$OUTPUT_DIR" ]; then
    openapi2jsonschema "https://raw.githubusercontent.com/kubernetes/kubernetes/${TAG}/api/openapi-spec/swagger.json" -o "${OUTPUT_DIR}" --stand-alone --kubernetes --expanded
  fi

  OUTPUT_DIR="${TAG}-standalone-strict"
  if [ ! -d "$OUTPUT_DIR" ]; then
    openapi2jsonschema "https://raw.githubusercontent.com/kubernetes/kubernetes/${TAG}/api/openapi-spec/swagger.json" -o "${OUTPUT_DIR}" --stand-alone --kubernetes --expanded --strict
  fi
done


